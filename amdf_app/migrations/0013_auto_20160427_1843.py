# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('amdf_app', '0012_auto_20160409_1527'),
    ]

    operations = [
        migrations.AddField(
            model_name='aboutus',
            name='history_en',
            field=models.TextField(default='', verbose_name='\u062a\u0627\u0631\u06cc\u062e\u0686\u0647'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='aboutus',
            name='project_en',
            field=models.TextField(default='', verbose_name='\u0645\u0631\u0627\u062d\u0644 \u0627\u0646\u062c\u0627\u0645 \u067e\u0631\u0648\u0698\u0647'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='aboutus',
            name='titleproject_en',
            field=models.CharField(default='', max_length=500, verbose_name='\u0639\u0646\u0648\u0627\u0646 \u0645\u0631\u0627\u062d\u0644 \u0627\u0646\u062c\u0627\u0645 \u067e\u0631\u0648\u0698\u0647 '),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='question',
            name='answer_en',
            field=models.TextField(default='', verbose_name='\u062c\u0648\u0627\u0628'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='question',
            name='question_en',
            field=models.CharField(default='', max_length=500, verbose_name='\u0633\u0648\u0627\u0644'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='team',
            name='name_en',
            field=models.CharField(default='', max_length=500, verbose_name='\u0646\u0627\u0645'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='team',
            name='title_en',
            field=models.CharField(default='', max_length=500, verbose_name='\u0639\u0646\u0648\u0627\u0646'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='textattrs',
            name='value_en',
            field=models.TextField(default='', verbose_name='\u0645\u0642\u062f\u0627\u0631'),
            preserve_default=False,
        ),
    ]
