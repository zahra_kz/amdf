# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('amdf_app', '0013_auto_20160427_1843'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('small_img', models.ImageField(help_text=b'size = 520px * 280px', upload_to=b'images/activities/', verbose_name='\ufe96\ufebb\ufeed\u06cc\ufead \u06a9\ufeed\u0686\u06a9')),
                ('big_img', models.ImageField(help_text=b'size = 520px * 280px', upload_to=b'images/activities/', verbose_name='\ufe96\ufebb\ufeed\u06cc\ufead \u06a9\ufeed\u0686\u06a9')),
                ('created_time', models.DateTimeField(auto_now_add=True, verbose_name='created time')),
                ('product', models.ForeignKey(verbose_name='\ufead\ufeed\u06cc\ufea9\ufe8d\ufea9', to='amdf_app.Product')),
            ],
            options={
                'ordering': ['created_time'],
                'verbose_name': '\u062a\u0635\u0648\u06cc\u0631 \u0645\u062d\u0635\u0648\u0644\u0627\u062a',
                'verbose_name_plural': '\u062a\u0635\u0648\u06cc\u0631 \u0645\u062d\u0635\u0648\u0644\u0627\u062a',
            },
        ),
    ]
