# -*- coding: UTF-8 -*-
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.core.validators import validate_email
from rest_framework import viewsets, routers, mixins, status
from rest_framework.reverse import reverse
from rest_framework.decorators import list_route, detail_route
from rest_framework.response import Response
from amdf_app.models import *
from serializers import *

def home(request,lang='fa'):
    language_mapping = {'en': 'en'}
    selected_lang = language_mapping.get(lang, 'fa')
    c={}
    c["lang"] = selected_lang
    c['inter'] = ProductCategory.objects.filter(key=u'دکوراسیون داخلی')
    c['workshop'] = ProductCategory.objects.filter(key=u'کارگاه')
    return render_to_response('index.html',c)

def questions(request,lang='fa'):
    language_mapping = {'en': 'en'}
    selected_lang = language_mapping.get(lang, 'fa')
    c={}
    c["lang"] = selected_lang
    return render_to_response('questions.html',c)

def contact(request,lang='fa'):
    language_mapping = {'en': 'en'}
    selected_lang = language_mapping.get(lang, 'fa')
    c={}
    c["lang"] = selected_lang
    return render_to_response('contact.html',c)

def products(request,lang='fa'):
    language_mapping = {'en': 'en'}
    selected_lang = language_mapping.get(lang, 'fa')
    c={}
    c["lang"] = selected_lang
    return render_to_response('gallery.html',c)

def ourproducts(request,lang='fa',id=1):
    language_mapping = {'en': 'en'}
    selected_lang = language_mapping.get(lang, 'fa')
    c={}
    c["lang"] = selected_lang
    parentcat=ProductCategory.objects.get(id=id)
    productSub=ProductSubCategory.objects.filter(parent=parentcat)
    c['categories'] = productSub
    c['products'] = Product.objects.filter(product=productSub)
    return render_to_response('ourproducts.html',c)

def aboutus (request,lang='fa'):
    language_mapping = {'en': 'en'}
    selected_lang = language_mapping.get(lang, 'fa')
    c={}
    c["lang"] = selected_lang
    c['aboutus'] = Aboutus.objects.all()[0]
    c['team']=Team.objects.all()
    return render_to_response('aboutus.html',c)

def product(request,id,lang='fa'):
    language_mapping = {'en': 'en'}
    selected_lang = language_mapping.get(lang, 'fa')
    c={}
    c["lang"] = selected_lang
    c['product']= Product.objects.get(id=id)
    c['images'] = ProductImage.objects.filter(product=c['product'])
    return render_to_response('product.html',c)

def gallery(request,lang='fa'):
    language_mapping = {'en': 'en'}
    selected_lang = language_mapping.get(lang, 'fa')
    c={}
    c["lang"] = selected_lang
    c['imgs'] = Gallery.objects.all()
    return render_to_response('gallery.html',c)

class QuestionViewSet(
        mixins.ListModelMixin,
        mixins.RetrieveModelMixin,
        viewsets.GenericViewSet):
    model = Question
    serializer_class = QuestionSerializer
    queryset = Question.objects.all()

class GalleryViewSet(
        mixins.ListModelMixin,
        mixins.RetrieveModelMixin,
        viewsets.GenericViewSet):
    model = Gallery
    serializer_class = GallerySerializer
    queryset = Gallery.objects.all()

router = routers.DefaultRouter()
router.register(r'question', QuestionViewSet, 'Question')
router.register(r'gallery', GalleryViewSet, 'Gallery')
