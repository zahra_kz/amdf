# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('amdf_app', '0005_auto_20160216_1604'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='product',
            options={'verbose_name': '\u0645\u062d\u0635\u0648\u0644\u0627\u062a', 'verbose_name_plural': '\u0645\u062d\u0635\u0648\u0644\u0627\u062a'},
        ),
    ]
