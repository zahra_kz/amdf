from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render_to_response, redirect
from django.contrib import messages
from django.template import RequestContext
from django import forms
from django.conf.urls import patterns, url
import django_jalali.admin as jadmin

from amdf_app.models import *

class ProductImageInline(admin.TabularInline):
    model = ProductImage
    extra = 3
#************* Questoin Admin *****************
class QuestionAdmin(admin.ModelAdmin):
    formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }

    class Media:
        js = ('ckeditor/ckeditor.js',)

#************* TextAttrs Admin *****************
class TextAttrsAdmin(admin.ModelAdmin):
    formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }

    class Media:
        js = ('ckeditor/ckeditor.js',)


#************* Product Admin *****************
class ProductAdmin(admin.ModelAdmin):
    inlines = [ ProductImageInline, ]
    formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }

    class Media:
        js = ('ckeditor/ckeditor.js',)

#************* ProductCategory Admin *****************
class ProductCategorytAdmin(admin.ModelAdmin):
    formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }

    class Media:
        js = ('ckeditor/ckeditor.js',)

#************* ProductCategory Admin *****************
class ProductUsertAdmin(admin.ModelAdmin):
    formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }

    class Media:
        js = ('ckeditor/ckeditor.js',)


admin.site.register(Gallery)
admin.site.register(ProductCategory,ProductCategorytAdmin)
admin.site.register(Product,ProductAdmin)
admin.site.register(ProductUser,ProductUsertAdmin)
admin.site.register(Question,QuestionAdmin)
admin.site.register(TextAttrs,TextAttrsAdmin)
admin.site.register(PageHeader)
admin.site.register(Aboutus,ProductUsertAdmin)
admin.site.register(Team)
admin.site.register(ProductSubCategory)


