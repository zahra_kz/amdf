# -*- coding: utf-8 -*-
from django.db import models
from datetime import datetime
from django.utils.translation import ugettext_lazy as _


class Gallery(models.Model):
    small_img = models.ImageField(default='images/gallery/defaults.jpg', upload_to='images/gallery/',verbose_name=_(u'تصویر کوچک'))
    large_img = models.ImageField(default='images/gallery/defaults.jpg', upload_to='images/gallery/',verbose_name=_(u'تصویر بزرگ'))
    alt = models.CharField(default='', max_length=100, verbose_name=_(u'توضیحات تصویر'))
    upload_date = models.DateTimeField(default=datetime.now, blank=True)

    def __unicode__(self):
        return self.alt

    class Meta:
        ordering = ['upload_date']
        verbose_name = _(u'تصاویر')
        verbose_name_plural = _(u'تصاویر')
#******************End Of Galley ********************

class ProductCategory(models.Model):
    KEYS = (
        (u'کارگاه', _(u'کارگاه')),
        (u'دکوراسیون داخلی', _(u'دکوراسیون داخلی')),
    )

    key = models.CharField(max_length=100, choices=KEYS, verbose_name=_(u'دسته اصلی'))
    name = models.CharField(default='other', max_length=50,verbose_name=_(u'زیر دسته بندی اصلی'))
    upload_date = models.DateTimeField(default=datetime.now, blank=True)

    def __unicode__(self):
        return self.key+" > "+self.name

    class Meta:
        ordering = ['name']
        verbose_name = _(u'دسته بندی محصولات')
        verbose_name_plural = _(u'دسته بندی محصولات')

#******************End of Product Category ********************

class ProductSubCategory(models.Model):
    parent = models.ForeignKey(ProductCategory, max_length=100, verbose_name=_(u'دسته اصلی'))
    name = models.CharField(default='other', max_length=50,verbose_name=_(u'زیر دسته بندی'))
    upload_date = models.DateTimeField(default=datetime.now, blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name = _(u'زیر دسته بندی محصولات')
        verbose_name_plural = _(u'زیر دسته بندی محصولات')

#******************End of Product Category ********************
class Product(models.Model):
    product = models.ForeignKey(ProductSubCategory,verbose_name=_(u'زیر دسته بندی محصولات'))
    small_img = models.ImageField(upload_to='images/product/', verbose_name=_(u'تصویر کوچک'))
    large_img = models.ImageField(upload_to='images/product/', verbose_name=_(u'تصویر بزرگ'))
    name = models.CharField(max_length=100, verbose_name=_(u'نام محصول'))
    model = models.CharField(max_length=100, verbose_name=_(u'مدل محصول'))
    spicification = models.TextField(verbose_name=_(u'مشخصات'))
    price = models.IntegerField(default=0,verbose_name=_(u'قیمت مدل'))
    descryption = models.TextField(verbose_name=_(u'توضیحات'))

    def __unicode__(self):
        return u'"{0}" "{1}"'.format(self.name,self.model)

    class Meta:
        verbose_name = _(u'محصولات')
        verbose_name_plural = _(u'محصولات')

class ProductImage(models.Model):
     product = models.ForeignKey(Product,verbose_name=_(u'محصول'))
     small_img = models.ImageField(upload_to='images/activities/',verbose_name=_(u'ﺖﺻﻭیﺭ کﻭچک'),help_text='size = 520px * 280px')
     big_img = models.ImageField(upload_to='images/activities/',verbose_name=_(u'ﺖﺻﻭیﺭ کﻭچک'),help_text='size = 520px * 280px')
     created_time = models.DateTimeField(auto_now_add=True, verbose_name=_('created time'))

     def __unicode__(self):
          return self.product.name

     class Meta:
          ordering = ['created_time']
          verbose_name = _(u'تصویر محصولات')
          verbose_name_plural = _(u'تصویر محصولات')

#******************End of Product ********************

class ProductUser(models.Model):
    REGISTER_TYPE = (
        (1, u'رزور'),
        (2, u'پرداخت شده'),
    )
    product = models.ForeignKey(Product, verbose_name=_(u'محصولات'))
    full_name = models.CharField(max_length=100, verbose_name=_(u'نام و نام خانوادگی'))
    email = models.EmailField(verbose_name=_(u'ایمل'))
    mobile = models.CharField(max_length=50, verbose_name=_(u'موبایل'))
    register_type = models.IntegerField(default=1, choices=REGISTER_TYPE, verbose_name=_(u'نوع ثبت'))
    authority_code = models.CharField(max_length=200, verbose_name=_('authority code'), blank=True, null=True)
    ref_id = models.CharField(max_length=200, verbose_name=_('ref_id'), blank=True, null=True)
    created_time = models.DateTimeField(auto_now_add=True, verbose_name=_('created time'))

    def __unicode__(self):
        return self.period_instance.name

    class Meta:
        verbose_name = _(u'مشتریان')
        verbose_name_plural = _(u'مشتریان')

#******************End of Period Instance User model ********************
class Question(models.Model):
    question = models.CharField(max_length=500,verbose_name=_(u'سوال'))
    question_en = models.CharField(max_length=500,verbose_name=_(u'سوال'))
    answer = models.TextField(verbose_name=_(u'جواب'))
    answer_en = models.TextField(verbose_name=_(u'جواب'))

    def __unicode__(self):
        return self.question

    class Meta:
        verbose_name = _(u'سوالات')
        verbose_name_plural = _(u'سوالات')

#******************End of Question model ********************
class TextAttrs(models.Model):
    KEYS = (
        ('PRODUCT_DESCRIPTION', _(u'توضیحات در مورد محصولات')),
        ('HOW_TO_Buy', _(u'نحوه خرید')),
        ('PROPERTIES', _(u'ویژگی های ما')),
    )

    key = models.CharField(max_length=100, choices=KEYS, unique=True, verbose_name=_(u'مشخصه'))
    value = models.TextField(verbose_name=_(u'مقدار'))
    value_en = models.TextField(verbose_name=_(u'مقدار'))


#******************End of Text Attrs model ********************
class PageHeader(models.Model):
    KEYS=(
        ('About', _(u'صفحه درباره ما')),
        ('Gallery', _(u'صفحه تصاویر')),
        ('Contact', _(u'صفحه درباره ما')),
        ('Product', _(u'صفحه محصولات')),
        ('Question', _(u'صفحه سوالات')),
    )
    page = models.CharField(max_length=100, choices=KEYS, verbose_name=_(u'صفحه'))
    image = models.ImageField(blank=True, null=True, upload_to='images/header/', verbose_name=_(u'تصویر'),help_text=_('size(1400*714)'))

    def __unicode__(self):
        return self.page

    class Meta:
        verbose_name =_(u'هدر صفحات')
        verbose_name_plural =_(u'هدر صفحات')

#******************End of Pageheader model ********************
class Aboutus(models.Model):
    titlehistory = models.CharField(max_length=500,verbose_name=_(u' عنوان تاریخچه '))
    history = models.TextField(verbose_name=_(u'تاریخچه'))
    history_en = models.TextField(verbose_name=_(u'تاریخچه'))
    titleproject = models.CharField(max_length=500,verbose_name=_(u'عنوان مراحل انجام پروژه '))
    titleproject_en = models.CharField(max_length=500,verbose_name=_(u'عنوان مراحل انجام پروژه '))
    project = models.TextField(verbose_name=_(u'مراحل انجام پروژه'))
    project_en = models.TextField(verbose_name=_(u'مراحل انجام پروژه'))


    class Meta:
        verbose_name = _(u'درباره ما')
        verbose_name_plural = _(u'درباره ما')
#******************End of Aboutus model ********************
class Team(models.Model):
    name = models.CharField(max_length=500,verbose_name=_(u'نام'))
    name_en = models.CharField(max_length=500,verbose_name=_(u'نام'))
    title = models.CharField(max_length=500,verbose_name=_(u'عنوان'))
    title_en = models.CharField(max_length=500,verbose_name=_(u'عنوان'))
    image = models.ImageField(blank=True, null=True, upload_to='images/team/', verbose_name=_(u'تصویر'),help_text=_('size(250*250)'))


#******************End of Team model ********************





