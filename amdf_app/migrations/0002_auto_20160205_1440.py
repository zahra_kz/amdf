# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('amdf_app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('full_name', models.CharField(max_length=100, verbose_name='\u0646\u0627\u0645 \u0648 \u0646\u0627\u0645 \u062e\u0627\u0646\u0648\u0627\u062f\u06af\u06cc')),
                ('email', models.EmailField(max_length=254, verbose_name='\u0627\u06cc\u0645\u0644')),
                ('mobile', models.CharField(max_length=50, verbose_name='\u0645\u0648\u0628\u0627\u06cc\u0644')),
                ('register_type', models.IntegerField(default=1, verbose_name='\u0646\u0648\u0639 \u062b\u0628\u062a', choices=[(1, '\u0631\u0632\u0648\u0631'), (2, '\u067e\u0631\u062f\u0627\u062e\u062a \u0634\u062f\u0647')])),
                ('authority_code', models.CharField(max_length=200, null=True, verbose_name='authority code', blank=True)),
                ('ref_id', models.CharField(max_length=200, null=True, verbose_name='ref_id', blank=True)),
                ('created_time', models.DateTimeField(auto_now_add=True, verbose_name='created time')),
            ],
            options={
                'verbose_name': '\u0645\u0634\u062a\u0631\u06cc\u0627\u0646',
                'verbose_name_plural': '\u0645\u0634\u062a\u0631\u06cc\u0627\u0646',
            },
        ),
        migrations.RemoveField(
            model_name='periodinstanceuser',
            name='product',
        ),
        migrations.RenameField(
            model_name='product',
            old_name='product_type',
            new_name='product',
        ),
        migrations.DeleteModel(
            name='PeriodInstanceUser',
        ),
        migrations.AddField(
            model_name='productuser',
            name='product',
            field=models.ForeignKey(verbose_name='\u0645\u062d\u0635\u0648\u0644\u0627\u062a', to='amdf_app.Product'),
        ),
    ]
