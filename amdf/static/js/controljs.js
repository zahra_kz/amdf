angular.module('imageListPagination', ['ngSanitize']).config(function($interpolateProvider,$httpProvider) {
              $httpProvider.defaults.headers.common['X-CSRFToken'] = csrftoken;
              $interpolateProvider.startSymbol('{$').endSymbol('$}');
});


angular.module('imageListPagination').controller('custCtrl',['$scope', "$http", function($scope, $http){

$scope.customer=[];
	$http.get('/api/customer/')
	  .success(function(response) {
		$scope.customer = response;
	});
}]).controller('questionCtrl',['$scope', "$http", function($scope, $http){

$scope.questions=[];
	$http.get('/api/question/')
	  .success(function(response) {
		$scope.questions = response;
	});

}]).controller('galleryCtrl',['$scope', "$http", function($scope, $http){
	$scope.imgs=[];
	$http.get('/api/gallery/')
	  .success(function(response) {
		$scope.imgs = response;
		console.log(response);
	});

}]).controller('booksCtrl',['$scope', "$http", '$filter', function($scope, $http, $filter){

	$scope.press=[];
	$scope.pt='BOOK';
	$http.get('/api/press/')
	  .success(function(response) {
		$scope.press = response;

	});
}]).controller('indexCtrl',['$scope', "$http", function($scope, $http){
	$scope.News=[];
	$http.get('/api/news/')
	  .success(function(response) {
		$scope.News = response;
		console.log(response);
		});
}]).controller('paymentCtrl',['$scope', "$http", function($scope, $http){
	$scope.online_payment=function(id){
	$scope.regtype=1;
	if($scope.register_type==true){
		$scope.regtype=2;
	}
 		$http({
 		method:"POST",
 		url:'/api/period_instance/'+id+'/register/',
 		data:{"name":$scope.first_name,
 			"last_name":$scope.last_name,
 			"email":$scope.email,
 			"mobile":$scope.mobile,
 			"register_type":$scope.regtype
 		},headers:{'Content-Type':'application/json;charset=utf-8'}
 		}).success(function(data){
			console.log(data);
                        if (typeof data.url != 'undefined'){
        			window.location=data.url;
			}
			else{
				$scope.err='پیش ثبت نام شما با موفقیت انجام شد، به زودی با شما تماس گرفته خواهد شد.';
			}

 		})
 		.error(function(err){
 			console.log(err);
                        $scope.err=err.msg;
      	
 		});
 	};
 	$scope.online_payment2=function(id){
	$scope.regtype=3;
 		$http({
 		method:"POST",
 		url:'/api/period_instance/'+id+'/register/',
 		data:{
 			"email":$scope.email1,
 			"mobile":$scope.mobile1,
 			"register_type":$scope.regtype
 		},headers:{'Content-Type':'application/json;charset=utf-8'}
 		}).success(function(data){
			if (typeof data.url != 'undefined'){
	 			window.location=data.url;
			}else{
			$scope.err="کاربری با این مشخصات وجود ندارد.";
			}	
		 })
 		.error(function(err){
 			console.log(err);
			$scope.err="کاربری با این مشخصات وجود ندارد.";
			alert("کاربری با این مشخصات وجود ندارد.");
 		});
 	};
}]);
