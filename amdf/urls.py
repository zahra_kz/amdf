"""amdf URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from amdf_app.views import *

urlpatterns = [
    url(r'^$', 'amdf_app.views.home', name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^questions/$', 'amdf_app.views.questions', name='questions'),
    url(r'^contact/$', 'amdf_app.views.contact', name='contact'),
    url(r'^gallery/$', 'amdf_app.views.gallery', name='gallery'),
    url(r'^ourproducts/(?P<id>\d{1,2})/$', 'amdf_app.views.ourproducts', name='ourproducts'),
    url(r'^product/(?P<id>\w+)/$', 'amdf_app.views.product', name='product'),
    url(r'^aboutus/$', 'amdf_app.views.aboutus', name='aboutus'),
    url(r'^portfolio/$', 'amdf_app.views.product', name='product'),
    url(r'^api/', include(router.urls)), # rest urls
]
