# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('amdf_app', '0010_auto_20160409_1518'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productcategory',
            name='key',
            field=models.CharField(unique=True, max_length=100, verbose_name='\u062f\u0633\u062a\u0647 \u0627\u0635\u0644\u06cc', choices=[('\u06a9\u0627\u0631\u06af\u0627\u0647', '\u06a9\u0627\u0631\u06af\u0627\u0647'), ('\u062f\u06a9\u0648\u0631\u0627\u0633\u06cc\u0648\u0646 \u062f\u0627\u062e\u0644\u06cc', '\u062f\u06a9\u0648\u0631\u0627\u0633\u06cc\u0648\u0646 \u062f\u0627\u062e\u0644\u06cc')]),
        ),
    ]
