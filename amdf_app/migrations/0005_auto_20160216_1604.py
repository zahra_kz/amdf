# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('amdf_app', '0004_auto_20160209_1359'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='large_img',
            field=models.ImageField(default='', upload_to=b'images/product/', verbose_name='\u062a\u0635\u0648\u06cc\u0631 \u0628\u0632\u0631\u06af'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='small_img',
            field=models.ImageField(default='', upload_to=b'images/product/', verbose_name='\u062a\u0635\u0648\u06cc\u0631 \u06a9\u0648\u0686\u06a9'),
            preserve_default=False,
        ),
    ]
