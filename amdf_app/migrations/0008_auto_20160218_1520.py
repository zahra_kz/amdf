# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('amdf_app', '0007_aboutus_team'),
    ]

    operations = [
        migrations.AddField(
            model_name='aboutus',
            name='titlehistory',
            field=models.CharField(default='', max_length=500, verbose_name=' \u0639\u0646\u0648\u0627\u0646 \u062a\u0627\u0631\u06cc\u062e\u0686\u0647 '),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='aboutus',
            name='titleproject',
            field=models.CharField(default='', max_length=500, verbose_name='\u0639\u0646\u0648\u0627\u0646 \u0645\u0631\u0627\u062d\u0644 \u0627\u0646\u062c\u0627\u0645 \u067e\u0631\u0648\u0698\u0647 '),
            preserve_default=False,
        ),
    ]
