# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('amdf_app', '0008_auto_20160218_1520'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductSubCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'other', max_length=50, verbose_name='\u0632\u06cc\u0631 \u062f\u0633\u062a\u0647 \u0628\u0646\u062f\u06cc')),
                ('upload_date', models.DateTimeField(default=datetime.datetime.now, blank=True)),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': '\u0632\u06cc\u0631 \u062f\u0633\u062a\u0647 \u0628\u0646\u062f\u06cc \u0645\u062d\u0635\u0648\u0644\u0627\u062a',
                'verbose_name_plural': '\u0632\u06cc\u0631 \u062f\u0633\u062a\u0647 \u0628\u0646\u062f\u06cc \u0645\u062d\u0635\u0648\u0644\u0627\u062a',
            },
        ),
        migrations.AddField(
            model_name='productcategory',
            name='key',
            field=models.CharField(default='', max_length=100, verbose_name='\u062f\u0633\u062a\u0647 \u0627\u0635\u0644\u06cc', choices=[(b'Workshop', '\u06a9\u0627\u0631\u06af\u0627\u0647'), (b'InterDecor', '\u062f\u06a9\u0648\u0631\u0627\u0633\u06cc\u0648\u0646 \u062f\u0627\u062e\u0644\u06cc')]),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='product',
            name='product',
            field=models.ForeignKey(verbose_name='\u0632\u06cc\u0631 \u062f\u0633\u062a\u0647 \u0628\u0646\u062f\u06cc \u0645\u062d\u0635\u0648\u0644\u0627\u062a', to='amdf_app.ProductSubCategory'),
        ),
        migrations.AlterField(
            model_name='productcategory',
            name='name',
            field=models.CharField(default=b'other', max_length=50, verbose_name='\u0632\u06cc\u0631 \u062f\u0633\u062a\u0647 \u0628\u0646\u062f\u06cc \u0627\u0635\u0644\u06cc'),
        ),
        migrations.AddField(
            model_name='productsubcategory',
            name='parent',
            field=models.ForeignKey(verbose_name='\u062f\u0633\u062a\u0647 \u0627\u0635\u0644\u06cc', to='amdf_app.ProductCategory', max_length=100),
        ),
    ]
