# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('amdf_app', '0002_auto_20160205_1440'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='gallery',
            name='cat_id',
        ),
        migrations.DeleteModel(
            name='Category',
        ),
    ]
