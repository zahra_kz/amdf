# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'other', max_length=50, verbose_name='\u0646\u0627\u0645 \u062f\u0633\u062a\u0647 \u0628\u0646\u062f\u06cc \u062a\u0635\u0627\u0648\u06cc\u0631')),
                ('upload_date', models.DateTimeField(default=datetime.datetime.now, blank=True)),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': '\u062f\u0633\u062a\u0647 \u0628\u0646\u062f\u06cc \u062a\u0635\u0627\u0648\u06cc\u0631',
                'verbose_name_plural': '\u062f\u0633\u062a\u0647 \u0628\u0646\u062f\u06cc \u062a\u0635\u0627\u0648\u06cc\u0631',
            },
        ),
        migrations.CreateModel(
            name='Gallery',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('small_img', models.ImageField(default=b'images/gallery/defaults.jpg', upload_to=b'images/gallery/', verbose_name='\u062a\u0635\u0648\u06cc\u0631 \u06a9\u0648\u0686\u06a9')),
                ('large_img', models.ImageField(default=b'images/gallery/defaults.jpg', upload_to=b'images/gallery/', verbose_name='\u062a\u0635\u0648\u06cc\u0631 \u0628\u0632\u0631\u06af')),
                ('alt', models.CharField(default=b'', max_length=100, verbose_name='\u062a\u0648\u0636\u06cc\u062d\u0627\u062a \u062a\u0635\u0648\u06cc\u0631')),
                ('upload_date', models.DateTimeField(default=datetime.datetime.now, blank=True)),
                ('cat_id', models.ForeignKey(verbose_name='\u062f\u0633\u062a\u0647 \u0628\u0646\u062f\u06cc \u062a\u0635\u0648\u06cc\u0631', to='amdf_app.Category')),
            ],
            options={
                'ordering': ['upload_date'],
                'verbose_name': '\u062a\u0635\u0627\u0648\u06cc\u0631',
                'verbose_name_plural': '\u062a\u0635\u0627\u0648\u06cc\u0631',
            },
        ),
        migrations.CreateModel(
            name='PageHeader',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('page', models.CharField(max_length=100, verbose_name='\u0635\u0641\u062d\u0647', choices=[(b'About', '\u0635\u0641\u062d\u0647 \u062f\u0631\u0628\u0627\u0631\u0647 \u0645\u0627'), (b'Gallery', '\u0635\u0641\u062d\u0647 \u062a\u0635\u0627\u0648\u06cc\u0631'), (b'Contact', '\u0635\u0641\u062d\u0647 \u062f\u0631\u0628\u0627\u0631\u0647 \u0645\u0627'), (b'Product', '\u0635\u0641\u062d\u0647 \u0645\u062d\u0635\u0648\u0644\u0627\u062a'), (b'Question', '\u0635\u0641\u062d\u0647 \u0633\u0648\u0627\u0644\u0627\u062a')])),
                ('image', models.ImageField(help_text='size(1400*714)', upload_to=b'images/header/', null=True, verbose_name='\u062a\u0635\u0648\u06cc\u0631', blank=True)),
            ],
            options={
                'verbose_name': '\u0647\u062f\u0631 \u0635\u0641\u062d\u0627\u062a',
                'verbose_name_plural': '\u0647\u062f\u0631 \u0635\u0641\u062d\u0627\u062a',
            },
        ),
        migrations.CreateModel(
            name='PeriodInstanceUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('full_name', models.CharField(max_length=100, verbose_name='\u0646\u0627\u0645 \u0648 \u0646\u0627\u0645 \u062e\u0627\u0646\u0648\u0627\u062f\u06af\u06cc')),
                ('email', models.EmailField(max_length=254, verbose_name='\u0627\u06cc\u0645\u0644')),
                ('mobile', models.CharField(max_length=50, verbose_name='\u0645\u0648\u0628\u0627\u06cc\u0644')),
                ('register_type', models.IntegerField(default=1, verbose_name='\u0646\u0648\u0639 \u062b\u0628\u062a', choices=[(1, '\u0631\u0632\u0648\u0631'), (2, '\u067e\u0631\u062f\u0627\u062e\u062a \u0634\u062f\u0647')])),
                ('authority_code', models.CharField(max_length=200, null=True, verbose_name='authority code', blank=True)),
                ('ref_id', models.CharField(max_length=200, null=True, verbose_name='ref_id', blank=True)),
                ('created_time', models.DateTimeField(auto_now_add=True, verbose_name='created time')),
            ],
            options={
                'verbose_name': '\u0645\u0634\u062a\u0631\u06cc\u0627\u0646',
                'verbose_name_plural': '\u0645\u0634\u062a\u0631\u06cc\u0627\u0646',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='\u0646\u0627\u0645 \u0645\u062d\u0635\u0648\u0644')),
                ('model', models.CharField(max_length=100, verbose_name='\u0645\u062f\u0644 \u0645\u062d\u0635\u0648\u0644')),
                ('spicification', models.TextField(verbose_name='\u0645\u0634\u062e\u0635\u0627\u062a')),
                ('price', models.IntegerField(default=0, verbose_name='\u0642\u06cc\u0645\u062a \u0645\u062f\u0644')),
                ('descryption', models.TextField(verbose_name='\u062a\u0648\u0636\u06cc\u062d\u0627\u062a')),
            ],
        ),
        migrations.CreateModel(
            name='ProductCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'other', max_length=50, verbose_name='\u0646\u0627\u0645 \u062f\u0633\u062a\u0647 \u0628\u0646\u062f\u06cc \u0645\u062d\u0635\u0648\u0644\u0627\u062a')),
                ('upload_date', models.DateTimeField(default=datetime.datetime.now, blank=True)),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': '\u062f\u0633\u062a\u0647 \u0628\u0646\u062f\u06cc \u0645\u062d\u0635\u0648\u0644\u0627\u062a',
                'verbose_name_plural': '\u062f\u0633\u062a\u0647 \u0628\u0646\u062f\u06cc \u0645\u062d\u0635\u0648\u0644\u0627\u062a',
            },
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.TextField(verbose_name='\u0633\u0648\u0627\u0644')),
                ('answer', models.TextField(verbose_name='\u062c\u0648\u0627\u0628')),
            ],
            options={
                'verbose_name': '\u0633\u0648\u0627\u0644\u0627\u062a',
                'verbose_name_plural': '\u0633\u0648\u0627\u0644\u0627\u062a',
            },
        ),
        migrations.CreateModel(
            name='TextAttrs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(unique=True, max_length=100, verbose_name='\u0645\u0634\u062e\u0635\u0647', choices=[(b'PRODUCT_DESCRIPTION', '\u062a\u0648\u0636\u06cc\u062d\u0627\u062a \u062f\u0631 \u0645\u0648\u0631\u062f \u0645\u062d\u0635\u0648\u0644\u0627\u062a'), (b'HOW_TO_Buy', '\u0646\u062d\u0648\u0647 \u062e\u0631\u06cc\u062f'), (b'PROPERTIES', '\u0648\u06cc\u0698\u06af\u06cc \u0647\u0627\u06cc \u0645\u0627')])),
                ('value', models.TextField(verbose_name='\u0645\u0642\u062f\u0627\u0631')),
            ],
        ),
        migrations.AddField(
            model_name='product',
            name='product_type',
            field=models.ForeignKey(verbose_name='\u062f\u0633\u062a\u0647 \u0628\u0646\u062f\u06cc \u0645\u062d\u0635\u0648\u0644\u0627\u062a', to='amdf_app.ProductCategory'),
        ),
        migrations.AddField(
            model_name='periodinstanceuser',
            name='product',
            field=models.ForeignKey(verbose_name='\u0645\u062d\u0635\u0648\u0644\u0627\u062a', to='amdf_app.Product'),
        ),
    ]
