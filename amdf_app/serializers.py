from django.core.validators import validate_email
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from rest_framework import serializers

from amdf_app.models import *

#************* Question Serializers *****************
class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question

#************* Gallery Serializers *****************
class GallerySerializer(serializers.ModelSerializer):
    large_img = serializers.SerializerMethodField('get_thumbnail_url')
    small_img = serializers.SerializerMethodField('get_thumbnail_url1')


    def get_thumbnail_url(self, obj):
        return '%s%s' % (settings.MEDIA_URL, obj.large_img)

    def get_thumbnail_url1(self,obj):
        return '%s%s' % (settings.MEDIA_URL, obj.small_img)

    class Meta:
        model = Gallery
        fields = ('id', 'small_img', 'large_img', 'alt', 'upload_date')
