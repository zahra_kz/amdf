# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('amdf_app', '0006_auto_20160216_1608'),
    ]

    operations = [
        migrations.CreateModel(
            name='Aboutus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('history', models.TextField(verbose_name='\u062a\u0627\u0631\u06cc\u062e\u0686\u0647')),
                ('project', models.TextField(verbose_name='\u0645\u0631\u0627\u062d\u0644 \u0627\u0646\u062c\u0627\u0645 \u067e\u0631\u0648\u0698\u0647')),
            ],
            options={
                'verbose_name': '\u062f\u0631\u0628\u0627\u0631\u0647 \u0645\u0627',
                'verbose_name_plural': '\u062f\u0631\u0628\u0627\u0631\u0647 \u0645\u0627',
            },
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=500, verbose_name='\u0646\u0627\u0645')),
                ('title', models.CharField(max_length=500, verbose_name='\u0639\u0646\u0648\u0627\u0646')),
                ('image', models.ImageField(help_text='size(250*250)', upload_to=b'images/team/', null=True, verbose_name='\u062a\u0635\u0648\u06cc\u0631', blank=True)),
            ],
        ),
    ]
